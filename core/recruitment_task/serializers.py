from django.core.validators import FileExtensionValidator
from rest_framework import serializers
import pandas as pd


class SummarySerializer(serializers.Serializer):
    column = serializers.CharField()
    sum = serializers.FloatField()
    avg = serializers.FloatField()


class ExcelFileSummarySerializer(serializers.Serializer):
    file = serializers.FileField(required=True, use_url=False, validators=[FileExtensionValidator(['xls', 'xlsx'])])
    columns = serializers.ListField(child=serializers.CharField(), write_only=True, required=True)
    summary = SummarySerializer(many=True, read_only=True)

    def validate(self, attrs):
        return attrs

    def process_file(self):
        summary = self.get_summary(self.validated_data['file'], self.validated_data['columns'])

        self.validated_data.update(**{"summary": summary})

    def get_summary(self, file, columns):

        # TODO search index of headers
        data = pd.read_excel(file, header=2)

        renamed_columns = {column: column.strip().lower() for column in data.columns}
        data.rename(columns=renamed_columns, inplace=True)

        # TODO search last valid index in file
        data = data.iloc[:-2, :]

        summary_objects = []
        for column in columns:
            try:
                col_data = data[column.lower()]
            except KeyError:
                # What if invalid column ? Actually do nothing
                continue

            if col_data.dtype == float:  # Technically excel stores all numeric data as float.
                summary_objects.append({
                    "column": column,
                    "sum": round(col_data.sum(), 2),
                    "avg": round(col_data.mean(), 2)
                })
        return summary_objects




        
