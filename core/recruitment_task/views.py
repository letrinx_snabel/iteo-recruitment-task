from rest_framework.parsers import JSONParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView

from core.recruitment_task.serializers import ExcelFileSummarySerializer


class ExcelFileSummaryView(APIView):
    parser_classes = [MultiPartParser]
    serializer_class = ExcelFileSummarySerializer

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.process_file()
        return Response(serializer.data)
