# Iteo Recruitment Task


## Description
Iteo recruitment task, that have to produce summary of given column names from excel file.

## Installation and run with docker

```
docker-compose up --build -d
```

## Installation and run

```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python manage.py runserver
```
